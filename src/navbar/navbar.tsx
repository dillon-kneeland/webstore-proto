import * as React from 'react';

class Navbar extends React.Component<{links: Array<{text: string, url: string}>}> {
    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <div className="navbar">
                {this.props.links.map((element, key) => {
                    return (
                        <a href={element.url}>{element.text}</a>
                    );
                })}
            </div>
        );
    }
}

export default Navbar;