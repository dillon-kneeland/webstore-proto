import * as React from 'react';

class NavbarItem extends React.Component<{text: string}, {active: boolean}> {
    constructor(props: any) {
        super(props);
        this.setState({active: false});
    }

    public render() {
        return (
            <div className="navbar-item" onClick={this.onClick}>
                <p>{this.props.text}</p>
            </div>
        )
    }

    public onClick(e: React.MouseEvent) {
        e.preventDefault();
    }
}

export default NavbarItem;