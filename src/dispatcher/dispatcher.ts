import * as invariant from 'invariant'

const TOK_PREFIX: string = "TKID_";

export type TokenID = string;

class Dispatcher {
    private callbacks: Map<TokenID, (payload: object) => TokenID>;

    private _isDispatching: boolean;
    private isHandled: Map<TokenID, boolean>;
    private isPending: Map<TokenID, boolean>;
    private lastID: number;
    private pendingPayload: object;

    constructor() {
        this.callbacks = new Map();
        this._isDispatching = false;
        this.isHandled = new Map();
        this.isPending = new Map();
        this.lastID = 1;
        this.pendingPayload = {};
    }

    public register(callback: (payload: object) => void): TokenID {
        const id: string = TOK_PREFIX + this.lastID++;
        this.callbacks[id] = callback;
        return id;
    }
    public unregister(token: TokenID): void {
        invariant(
            this.callbacks.has(token),
            'Dispatcher.unregister(...): Token `%s` is not mapped to a registered callback.',
            token);
        this.callbacks.delete(token);
    }
    public waitFor(tokens: TokenID[]): void {
        invariant(
            this.isDispatching(),
            'Dispatcher.waitFor(...): Must be invoked while dispatching.'
        );
        tokens.forEach((id: TokenID) => {
            if (this.isPending[id]) {
                invariant(
                    this.isHandled[id],
                    'Dispatcher.waitFor(...): Circular dependency detected while waiting for `%s`.',
                    id
                );
                return;
            }
            invariant(
                this.callbacks[id],
                'Dispatcher.waitFor(...): Token `%s` is not mapped to a registered callback',
                id
            );
            this.invokeCallback(id);
        });
    }
    public isDispatching(): boolean {
        return this._isDispatching;
    }
    public dispatch(payload: object): void {
        invariant(
            !this._isDispatching,
            'Dispatch.dispatch(...): Cannot dispatch while currently dispatching.'
        );
        this.startDispatching(payload);
        try {
            this.callbacks.forEach((callback, id) => {
                if (this.isPending[id]) {
                    return;
                }
                this.invokeCallback(id);
            });
        } finally {
            this.stopDispatching();
        }
    }

    private startDispatching(payload: object): void {
        this.callbacks.forEach((c, id) => {
            this.isPending[id] = false;
            this.isHandled[id] = false;
        });
        this.pendingPayload = payload;
        this._isDispatching = true;
    }
    private invokeCallback(id: TokenID): void {
        this.isPending[id] = true;
        this.callbacks[id](this.pendingPayload);
        this.isHandled[id] = true;
    }
    private stopDispatching(): void {
        delete this.pendingPayload;
        this._isDispatching = false;
    }
}

export default new Dispatcher();